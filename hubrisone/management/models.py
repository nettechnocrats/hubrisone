from django.db import models

# Create your models here.

class Country(models.Model):
	cont_id 	= models.AutoField(primary_key= True, verbose_name= 'country')
	name 		= models.CharField(max_length=60, db_column='cont_name', verbose_name=('name'))
	is_valid	= models.CharField(max_length=2)

	def __str__(self):
		return str(self.cont_id)