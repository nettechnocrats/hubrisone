from django.shortcuts import render
from management.models import Country
from rest_framework.views import APIView
from rest_framework.response import Response
from management.serializers import CountrySerializer
# Create your views here.

class CountryDetailView(APIView):

	def get(self, request):

		queryset = Country.objects.all()
		queryset = {"Country":[{'cont_id':x.cont_id,'name':x.name,'is_valid':x.is_valid} for x in queryset]}
		
		return Response(queryset)

		