from django.urls import path
from management import views

app_name = 'management'

urlpatterns = [
    path('country_detail/', views.CountryDetailView.as_view())
]