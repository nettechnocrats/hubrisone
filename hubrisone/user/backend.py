from user.models import Profile



class LoginBackend:

    def authenticate(self, request):

    	device_id = request.data.get('device_id',None)
    	device_type = request.data.get('device_type',None)
    	pass_code = request.data.get('pass_code',None)
    	touch_id = request.data.get('touch_id',None)
    	
    	if pass_code:
    		try:
    			user_obj = Profile.objects.get(device_id = device_id, device_type = device_type, pass_code = pass_code)
    		except Profile.DoesNotExist:
    			return None
    	
    	if touch_id:
    		try:
    			user_obj = Profile.objects.get(device_id = device_id, device_type = device_type, touch_id = touch_id)
    		except Profile.DoesNotExist:
    			return None

    	return user_obj
    def get_user(self, user_id):
    	
    	user = Profile.objects.get(user_id = user_id)
    	
    	return user