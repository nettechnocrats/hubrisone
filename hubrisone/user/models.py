from django.db import models
from django.contrib.auth.models import User
from datetime import timedelta,datetime
from management.models import Country
from django.conf import settings
# Create your models here.



class Profile(models.Model):

	user			= models.OneToOneField(User, on_delete= models.CASCADE, primary_key= True)
	touch_id		= models.CharField(max_length= 255, null=True, blank=True)
	pass_code		= models.CharField(max_length= 6, null=True, blank=True)
	referrer_code	= models.CharField(max_length = 15, null = True, blank=True)
	mobile			= models.CharField(max_length=13)
	country			= models.CharField(max_length= 20)
	dob				= models.DateField()
	address			= models.CharField(max_length= 255)
	zip_code		= models.IntegerField()
	device_id		= models.CharField(max_length= 255)
	device_type		= models.CharField(max_length= 20)
	is_premium		= models.CharField(max_length= 2, default='N')

	def __str__(self):
		return self.user.username


class OtpVerification(models.Model):

	otp_id			= models.AutoField(primary_key= True, verbose_name= 'otp')
	created_time 	= models.DateTimeField(default= datetime.now())
	expire_time		= models.DateTimeField(default= datetime.now()+timedelta(minutes=2))
	mobile			= models.CharField(max_length= 13)
	otp 			= models.CharField(max_length= 10)
	is_for_pwd_reset= models.CharField(max_length= 2, default='N')
	is_verified		= models.CharField(max_length= 2, default='N')

	def __str__(self):
		return self.otp_ids

class EmailVerification(models.Model):

	emv_id			= models.AutoField(primary_key= True, verbose_name= 'email_verification')
	created_time 	= models.DateTimeField(default= datetime.now())
	email			= models.EmailField()
	email_hash		= models.CharField(max_length=250, default = '')
	is_for_pwd_reset= models.CharField(max_length= 2, default = 'N')
	is_verified		= models.CharField(max_length= 2, default = 'N' )

	def __str__(self):
		return self.emv_id


class Referral(models.Model):

	ref_id = models.AutoField(primary_key = True, verbose_name = 'referral')
	referee_code = models.CharField(max_length = 15, unique = True)
	user = models.ForeignKey(Profile, models.DO_NOTHING, db_column ='user')

	def __str__(self):
		return self.ref_id
