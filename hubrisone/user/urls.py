from django.urls import path,re_path
from user import views

app_name = 'user'

urlpatterns = [
    path('signup/', views.ProfileRecordView.as_view()),
    path('otp_verification/', views.VerificationView.as_view()),
    path('email_verification/', views.VerificationView.as_view()),
    path('pwd_reset_otp_verification/', views.VerificationView.as_view()),
    path('pwd_reset_email_verification/', views.VerificationView.as_view()),
    path('confirm_email/<email>/', views.VerificationView.as_view(), name= 'confirm_email'),
    path('check_email/', views.check_email, name= 'check_email'),
    path('check_mobile/', views.check_mobile, name= 'check_mobile'),
    path('password_reset/<email>/<password_reset>/', views.VerificationView.as_view(), name= 'password_reset_email'),
    path('login/', views.LogInView.as_view(),name='Login'),
    path('password_reset/', views.PasswordResetView.as_view(),name='Login'),
    path('check_username/', views.unique_username, name= 'check_username'),
]
