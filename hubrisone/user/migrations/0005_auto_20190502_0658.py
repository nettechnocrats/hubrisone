# Generated by Django 2.1.7 on 2019-05-02 06:58

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_auto_20190502_0653'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailverification',
            name='email_hash',
            field=models.CharField(default='', max_length=250),
        ),
        migrations.AlterField(
            model_name='emailverification',
            name='created_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 2, 6, 58, 42, 715579)),
        ),
        migrations.AlterField(
            model_name='otpverification',
            name='created_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 2, 6, 58, 42, 715208)),
        ),
        migrations.AlterField(
            model_name='otpverification',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 2, 7, 0, 42, 715233)),
        ),
    ]
