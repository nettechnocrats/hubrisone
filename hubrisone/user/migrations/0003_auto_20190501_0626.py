# Generated by Django 2.1.7 on 2019-05-01 06:26

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_auto_20190501_0620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailverification',
            name='created_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 1, 6, 26, 13, 737049)),
        ),
        migrations.AlterField(
            model_name='otpverification',
            name='created_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 1, 6, 26, 13, 736681)),
        ),
        migrations.AlterField(
            model_name='otpverification',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 1, 6, 28, 13, 736705)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='referrer_code',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
    ]
