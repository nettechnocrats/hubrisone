from rest_framework import serializers
from user.models import User, Profile
import datetime
from rest_framework.response import Response
class UserSerializer(serializers.ModelSerializer):

	class Meta:
		model = User	
		fields = (
					'first_name',
					'last_name',
					'username',
					'email',
				)

class ExtendedUserSerializer(serializers.ModelSerializer):

	user = UserSerializer(required = True)
	class Meta:
		model = Profile
		fields = (	'user',
					'referrer_code',
					'mobile',
					'country',
					'dob',
					'address',
					'zip_code',
					'device_id',
					'device_type',
					'touch_id',
					'pass_code'
					)

	

	def create(self, validated_data):

		dob = validated_data.pop('dob')
		user_data = validated_data.pop('user')
		user = UserSerializer.create(UserSerializer(), validated_data=user_data)
		profile, created = Profile.objects.update_or_create(user= user,
															referrer_code = validated_data.pop('referrer_code'),
															mobile = validated_data.pop('mobile'),
															country = validated_data.pop('country'),
															dob = dob,
															address = validated_data.pop('address'),
															zip_code = validated_data.pop('zip_code'),
															device_id = validated_data.pop('device_id'),
															device_type = validated_data.pop('device_type'),
															touch_id = validated_data.pop('touch_id'),
															pass_code = validated_data.pop('pass_code'),
															)

		

		return profile