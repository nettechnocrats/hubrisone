import urllib.request
import urllib.parse
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.core.mail import send_mail
from user.models import EmailVerification


def sendSMS(apikey, numbers, message):

	data =  urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers,'message' : message})
	data = data.encode('utf-8')
	request = urllib.request.Request("https://api.textlocal.in/send/?")
	f = urllib.request.urlopen(request, data)
	fr = f.read()
	
	return(fr)

def send_email(request, email):
	
	current_site = get_current_site(request)
	domain = current_site.domain
	encoded_email = urlsafe_base64_encode(force_bytes(email)).decode()
	html_message = render_to_string('email_verification/email_confirmation.html',{'domain':domain,'email':encoded_email})
	mail = send_mail('Email Verification', '', 'deepakumredkar16@gmail.com', [email], fail_silently=False,html_message=html_message)

	return mail