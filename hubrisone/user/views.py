from django.shortcuts import render
# Create your views here.
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from user.models import Profile,EmailVerification, OtpVerification, Referral
from rest_framework import viewsets
from user.serializers import ExtendedUserSerializer
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.response import Response
from user.helper import sendSMS, send_email
from user.backend import LoginBackend
import random
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_text
from django.http import HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
import json
import datetime

class ProfileRecordView(APIView):
    """
    A class based view for creating records
    """
    def post(self, request):

        user = {}
        profile = {}
        user['first_name'] = request.data['first_name']
        user['last_name'] = request.data['last_name']
        username = request.data['username']
        user['username']  = username
        email = request.data['email']
        user['email'] = email
        profile['pass_code'] = request.data['pass_code']
        profile['user'] = user
        profile['referrer_code'] = request.data['referrer_code']
        mobile = request.data['mobile']
        profile['mobile'] = mobile
        profile['country'] = request.data['country']
        dob = request.data['dob']
        profile['dob'] = datetime.datetime.strptime(dob, '%d/%m/%Y').strftime('%Y-%m-%d')
        profile['address'] = request.data['address']
        profile['zip_code'] = request.data['zip_code']
        profile['device_id'] = request.data['device_id']
        profile['device_type'] =request.data['device_type']
        profile['touch_id'] = request.data['touch_id']

        if username:
            try:
                user_obj = User.objects.get(username = username)
                return Response({'status':'0','msg':'Username already in use!!'})
            except:
                user_obj = ''

        if email:
            try:
                EmailVerification.objects.filter(email=email, is_verified='Y')
            except:
                return Response({'status':'0','msg':'email is not verified please verify your email first'})
            try:
                user_obj = User.objects.get(email = email)
                return Response({'status':'0','msg':'email already in use!!'})
            except:
                user_obj = ''
                    
        if mobile:
            try:
                OtpVerification.objects.filter(mobile=mobile, is_verified='Y')
            except:
                return Response({'status':'0','msg':'mobile number is not verified please verify your mobile number first'})
            try:
                prof_obj = Profile.objects.get(mobile = mobile)
                return Response({'status':'0','msg':'mobile number already in use!!'})
            except:
                prof_obj = ''
            

        if not profile['touch_id'] and not profile['pass_code']:
            return Response({'status':'0','msg':'touch_id or passcode one of them is required'})

        serializer = ExtendedUserSerializer(data=profile)
        if serializer.is_valid():
            profile_created = serializer.create(validated_data=profile)
            code = random.randint(1000,9999)
            referee_code= str(username.upper())+str(code)
            Referral.objects.create(referee_code = referee_code, user = profile_created)
            token = Token.objects.create(user = profile_created.user)

            request.session['user'] = profile_created.user.id

            return Response({'status':'1','token':token.key},
                            status=status.HTTP_201_CREATED)

        return Response({'status':'0','msg':'Invalid data'})

class LogInView(APIView):

    def post(self, request):

        try:
            device_id = request.data['device_id']
        except:
            return Response({'status':'0','msg':'device_id is required'})

        try:
            device_type = request.data['device_type']
        except:
            return Response({'status':'0','msg':'device_type is required'})

        try:
            pass_code = request.data['pass_code']
            user_obj = LoginBackend.authenticate(self, request)
            try:

                token = Token.objects.get(user = user_obj.user.id)
                login(request, user_obj.user)
                return Response({'status':'1','msg':'correct Credentials','token':token.key})
            except:
                return Response({'status':'0','msg':'invalid Credentials'})

        except:
            touch_id = request.data['touch_id']
            user_obj = LoginBackend.authenticate(self, request)
            try:

                token = Token.objects.get(user = user_obj.user.id)
                login(request, user_obj.user)
                return Response({'status':'1','msg':'correct Credentials','token':token.key})
            except:
                return Response({'status':'0','msg':'invalid Credentials'})

class VerificationView(APIView):

    def post(self, request):

        email = request.data.get('email', None)
        mobile = request.data.get('mobile', None)
        password_reset = request.data.get('password_reset',None)
        if email:
            try:
                emv_obj= EmailVerification.objects.get(email=email,is_verified='Y')
                if password_reset:
                    send_email(request, email)

                    return Response({'status':'1','msg':'Email Verification for password reset has been sent'})
                else:
                    return Response({'status':'0','msg':'Email already exists. Please Try another email'})
            except:
                send_email(request, email)

                return Response({'status':'1','msg':'Email Verification has been sent'})

        if mobile:
            
            otp = random.randint(1000,9999)
            try:
                otp_obj= OtpVerification.objects.get(mobile=mobile)

                if otp_obj.is_verified == 'Y':
                    if password_reset:

                        # message = sendSMS('KgyO4lhA/ko-lSaL7bS13KGZXWob6jM8lctKlhiMYy', mobile, 'Your OTP is:'+str(otp))
                        # message = json.loads(message.decode("utf-8"))
                        # if message['status'] == 'failure':
                        #     return Response({'status':'0','msg':'Mobile Otp did not sent'})

                        # use above lines in production


                        otp_obj.otp = otp
                        otp_obj.is_verified = 'N'
                        otp_obj.save()
                        return Response({'status':'1', 'msg':'Mobile Otp sent','otp':otp})
                    else:

                        return Response({'status':'0','msg':'number already exists. Please Try another number'})
                else:

                    # message = sendSMS('KgyO4lhA/ko-lSaL7bS13KGZXWob6jM8lctKlhiMYy', mobile, 'Your OTP is:'+str(otp))
                    # message = json.loads(message.decode("utf-8"))
                    # if message['status'] == 'failure':
                    #     return Response({'status':'0','msg':'Mobile Otp did not sent'})
                    # use above line in production

                    try:
                        created_otp_obj = OtpVerification.objects.create(mobile=str(mobile))
                    except:

                        return Response({'status':'0','msg':'Mobile Otp did not sent','Location':e.__class__.__name__+' occured while creating OtpVerification data. \n Location: user.views.VerificationView'})


                    otp_obj.delete()
                    # return Response({'status':'1','msg':'Mobile Otp sent' })
                    return Response({'status':'1','msg':'Mobile Otp sent','otp':str(otp) })

            except:

                # message = sendSMS('KgyO4lhA/ko-lSaL7bS13KGZXWob6jM8lctKlhiMYy', mobile, 'Your OTP is:'+str(otp))
                # message = json.loads(message.decode("utf-8"))
                # if message['status'] == 'failure':
                #     return Response({'status':'0','msg':'Mobile Otp did not sent'})
                # use above line in production

                try:
                    created_otp_obj = OtpVerification.objects.create(mobile=mobile, otp=otp)
                except:
                    return Response({'status':'0','msg':'Mobile Otp did not sent','Location':e.__class__.__name__+' occured while creating OtpVerification data. \n Location: user.views.VerificationView'})

                return Response({'status':'1','msg':'Mobile Otp sent','otp':otp})

    def get(self, request,*args, **kwargs):

        try:
            email = request.parser_context['kwargs']['email']
        except:
            return Response({'status':'0','msg':'email is required'})
        try:
            password_reset = request.parser_context['kwargs']['password_reset']
        except:
            password_reset = ''

        email = force_text(urlsafe_base64_decode(email))

        if not password_reset:
            EmailVerification.objects.create(email=email,is_verified='Y')

        return HttpResponse('<h1>Verified!!</h1>')

@api_view()
def check_email(request):

    try:
        email= request.GET['email']
    except:
        return Response({'status':'0','msg':'email is required'})

    try:
        emv_obj= EmailVerification.objects.get(email=email, is_verified='Y')
        return Response({'status':'1','msg':'Email is confirmed'})
    except:
        return Response({'status':'0','msg':'Email is not verified'})


@api_view()
def check_mobile(request):

    try:
        mobile = request.GET['mobile']
    except:
        return Response({'status':'0','msg':'mobile is required'})

    try:
        otp = request.GET['otp']
    except:
        return Response({'status':'0','msg':'otp is required'})

    try:
        otp_obj = OtpVerification.objects.get(mobile=mobile, otp=otp)
        otp_obj.is_verified = 'Y'
        otp_obj.save()
        return Response({'status':'1','msg':'OTP Verified'})
    except:
        return Response({'status':'0','msg':'Incorrect OTP'})


@api_view(['POST'])
def unique_username(request):

    username = request.POST.get('username',None)
    try:
        if username:
            user_obj = User.objects.get(username = username)
            return Response({'status':'0','msg':'Username already in use!!'})
        else:
            return Response({'status':'0','msg':'username cannot be null'})
    except:
        return Response({'status':'1','msg':'Valid Username'})


class PasswordResetView(APIView):

    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def post(self, request):

        try:
            token = request.data['token']
        except:
            return Response({'status':'0','msg':'token is required'})

        try:
            pass_code = request.data['pass_code']
        except:
            return Response({'status':'0','msg':'pass_code is required'})

        try:
            token_obj = Token.objects.get(key = token)
            user_obj = Profile.objects.get(user_id = token_obj.user_id)

            if pass_code == user_obj.pass_code:
                return Response({'status':'0','msg':'pass_code should be different from previous one'})

            user_obj.pass_code = pass_code
            user_obj.save()

            return Response({'status':'1','msg':'passcode is changed'})
        except:
            return Response({'status':'0','msg':'Invalid Token'})
